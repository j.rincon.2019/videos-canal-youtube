import sys
import xml.etree.ElementTree as ET

def generate_html(video_links):
    html_content = "<!DOCTYPE html>\n<html>\n<head>\n<title>CursosWeb Videos</title>\n</head>\n<body>\n"
    html_content += "<h1>Videos del canal:</h1>\n"
    for title, link in video_links:
        html_content += f'<p><a href="{link}">{title}</a></p>\n'
    html_content += "</body>\n</html>"
    return html_content

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 ytparser.py <xml_file> > pagina.html")
        print()
        print("<xml_file>: XML file containing the YouTube channel data")
        sys.exit(1)

    xml_file = sys.argv[1]
    tree = ET.parse(xml_file)
    root = tree.getroot()

    video_links = []
    for entry in root.findall('{http://www.w3.org/2005/Atom}entry'):
        title = entry.find('{http://www.w3.org/2005/Atom}title').text.strip()
        link = entry.find('{http://www.w3.org/2005/Atom}link').attrib['href']
        video_links.append((title, link))

    html_content = generate_html(video_links)

    print(html_content)
